var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var _ = require('lodash');


var index = require('./routes/index');
var User = require('./models/user');

var users = [];

app.use(express.static('public'));
app.use('/lodash',express.static('node_modules/lodash'));
app.use('/', index);


io.on('connection', function (socket) {

    socket.on('disconnect', function () {

        /* verify if it was a client */
        if(socket.user) {
            users.splice(_.findIndex(users, {id: socket.user.id}), 1);

            io.emit('userDisonnected', socket.user.id);
        }

        console.log(users);
    });

    socket.on('connected', function () {

        socket.user = new User();

        console.log("users connected");

        io.emit('userConnected', socket.user.id);

        users.push(socket.user);

        console.log(users);
    });

    socket.on('updateVelocity', function (data) {
        io.emit('userUpdateVelocity', {
            userID: socket.user.id,
            valpha: data
        });
    });

});

http.listen(8080, "172.28.59.75", function () {
    console.log('listening on *:8080');
});
