/**
 * Application modules
 */
var app = (function () {

    var User = function(userId) {
        this.id = userId;
        this.circle = null;
    };

    var Satellite = function (center, parentRadius) {
        this.alpha = UMath.degreesToRadian(UMath.getRandomInt(1,360));
        this.center = center;
        this.parentRadius = parentRadius;

        this.pos = {
            x: null,
            y: null
        };

        this.radius = UMath.getRandomInt(18,25);
        this.valpha = 0.01;

        /* get position of sattelite with pythagore */
        this.getPos();
        
        this.color = "#000000";
        this.fill = "#ffffff";
    };

    Satellite.prototype.getPos = function() {
        this.pos.x = this.center.x + Math.sin(this.alpha) * this.parentRadius;
        this.pos.y = this.center.y + Math.cos(this.alpha) * this.parentRadius;
    };

    Satellite.prototype.render = function (ctx) {
        ctx.beginPath();
        ctx.arc(this.pos.x, this.pos.y, this.radius, 0, 2 * Math.PI, false);
        ctx.lineWidth = this.radius / 4;
        ctx.strokeStyle = this.color;
        ctx.stroke();
        ctx.fillStyle = this.fill;
        ctx.fill();

        /* Update position of satellite */
        this.alpha += this.valpha;
        this.getPos();
    };

    /**
     * Circle object
     * @param x
     * @param y
     * @param radius
     * @param hasSatellite
     * @param isFilled
     * @constructor
     */
    var Circle = function (x, y, radius, hasSatellite, isFilled) {
        this.pos = {
            x: x,
            y: y
        };

        this.hasSatellite = hasSatellite && true;
        this.isFilled = isFilled || false;
        this.radius = radius;
        this.color = "#000000";

        if (this.hasSatellite) {
            this.satellite = new Satellite(
                {
                    x: this.pos.x,
                    y: this.pos.y
                },
                this.radius
            );
        }
    };

    Circle.prototype.render = function (ctx) {
        ctx.beginPath();
        ctx.arc(this.pos.x, this.pos.y, this.radius, 0, 2 * Math.PI, false);
        ctx.lineWidth = 2;
        ctx.strokeStyle = this.color;
        ctx.stroke();

        if (this.isFilled) {
            ctx.fillStyle = this.color;
            ctx.fill();
        }

        /* render satellite (allow animation of it) */
        if (this.hasSatellite) {
            this.satellite.render(ctx);
        }
    };

    /**
     * App object
     * @type {{socket: null, w: null, h: null, center: {x: null, y: null}, circles: Array, initCircle: App.initCircle, init: App.init, render: App.render}}
     */
    var App = {
        socket: null,
        w: null,
        h: null,
        center: {
            x: null,
            y: null
        },
        offsetCircle: 180,
        circles: [],
        users: [],

        initCircle: function () {
            var circleCenter = new Circle(this.center.x, this.center.y, 100, false, true);
            this.circles.push(circleCenter);
        },

        onUserConnected: function(userId) {
            console.log("onUserConnected", userId);

            var user = new User(userId);
            var radius = (this.users.length > 0) ? (this.users.length + 1) * (this.offsetCircle / 1.40) : radius = this.offsetCircle;
            radius += UMath.getRandomInt(0,10);

            user.circle = new Circle(this.center.x, this.center.y, radius , true);

            this.users.push(user);
        },

        onUserDisonnected: function(userId) {
            console.log("onUserDisonnected");

            this.users.splice(_.findIndex(this.users, {id: userId}), 1);
        },

        onUserUpdateVelocity: function(data) {
            console.log("onUserUpdateVelocity", data);

            var user = this.users[_.findIndex(this.users, {id: data.userID})];
            console.log(data.valpha);

            user.circle.satellite.valpha = data.valpha;
        },

        init: function () {
            this.socket = io();

            this.socket.on('userConnected', this.onUserConnected.bind(this));
            this.socket.on('userDisonnected', this.onUserDisonnected.bind(this));
            this.socket.on('userUpdateVelocity', this.onUserUpdateVelocity.bind(this));

            this.canvas = document.getElementById("canvas");
            this.ctx = this.canvas.getContext('2d');
            this.w = this.canvas.width = window.innerWidth;
            this.h = this.canvas.height = window.innerHeight;
            this.center.x = this.canvas.width / 2;
            this.center.y = this.canvas.height / 2;

            this.initCircle();
            this.render();

        },

        drawMiddleCircle: function () {
            var midCircle = new Circle(this.center.x, this.center.y, 100, false, true);
            midCircle.render(this.ctx);
        },

        render: function () {
            this.ctx.clearRect(0, 0, this.w, this.h);

            this.drawMiddleCircle();

            var circle;

            for (var i = 0; i < this.users.length; i++) {
                circle = this.users[i].circle;
                circle.render(this.ctx);
            }

            requestAnimationFrame(this.render.bind(this));
        }
    };

    return App;
})();

app.init();

console.log(app);