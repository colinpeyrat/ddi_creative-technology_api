var UMath = (function() {
    var UMath = {
        getRandomInt: function (min, max) {
            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min +1)) + min;
        },

        degreesToRadian: function (value) {
            return value * (Math.PI / 180);
        },

        radianToDegrees: function (value) {
            return value / (Math.PI / 180);
        }
    };

    return UMath;
})();